﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nobble.Models
{
    [Table("Produit")]
    public class Produit
    {
        [Key]
        public int id { get; set; }
        [Required]
        [MaxLength(15)]
        public string libelle { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public float prix { get; set; }
        [Required]
        public bool is_option { get; set; }
    }
}