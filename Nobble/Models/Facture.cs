﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nobble.Models
{
    [Table("Facture")]
    public class Facture
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string user_id { get; set; }
        [Required]
        public DateTime date_facture { get; set; }

        [NotMapped]
        public List<Abonnement> abonnements { get; set; }

        [NotMapped]
        public float prixTotal { get; set; }
        [NotMapped]
        public bool isEmpty { get; set; }
    }
}