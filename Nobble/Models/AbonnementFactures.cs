﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nobble.Models
{
    [Table("AbonnementFactures")]
    public class AbonnementFactures
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int facture_id { get; set; }
        [Required]
        public int abonnement_id { get; set; }
    }
}