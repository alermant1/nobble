﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nobble.Models
{
    public class UserInfo
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string user_id { get; set; }
        [Required]
        public string nom { get; set; }
        [Required]
        public string prenom { get; set; }
        [Required]
        public string adresse { get; set; }
        [Required]
        public string cdp { get; set; }
        [Required]
        public string ville { get; set; }
    }
}