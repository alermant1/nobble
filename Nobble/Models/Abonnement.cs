﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nobble.Models
{
    [Table("Abonnement")]
    public class Abonnement
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string user_id { get; set; }
        [Required]
        public int produit_id { get; set; }
        [Required]
        public float prix { get; set; }
        [Required]
        public DateTime date_achat { get; set; }
        public int frequence { get; set; }
        [Required]
        public bool is_actif { get; set; }
        [Required]
        public bool is_option { get; set; }
        [NotMapped]
        public Produit produit { get; set; }
    }
}