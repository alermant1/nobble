(function($) {

    $('#headers .panier').off().on('click', function() {
        $("#headers .panier").toggleClass('active');
        $("#headers .view-panier").toggleClass('active');
    });

    $('#headers #name-user-co').off().on('click', function() {
        $(this).toggleClass('active');
        $(this).find(".down-menu").toggleClass('active');
    });

    $('#headers #menu-mobile-bt').off().on('click', function() {
        $(this).toggleClass('active');
        $("#headers .menu-main").toggleClass('active');
    });

    $("#headers .view-panier").mCustomScrollbar();
    $(".admin-panel .content.mes-factures , .admin-panel .content.all-fact").mCustomScrollbar();



})(jQuery);