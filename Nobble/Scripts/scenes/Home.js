(function ($) {

    $(".block-smartphone .phone.one").delay(300).queue(function () { $(this).addClass('active') });
    $(".block-smartphone .phone.two").delay(600).queue(function () { $(this).addClass('active') });

    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 300) {
            $(".block-iphonered.one .picture-iphone .picture").delay(300).queue(function () { $(this).addClass('active') });
            $(".block-iphonered.two .picture-iphone .picture").delay(600).queue(function () { $(this).addClass('active') });
        }
    });

})(jQuery);