(function ($) {

    $('.block-forfait .forfait .vignette').off().on('click', function () {
        $('.block-forfait .forfait .vignette').removeClass("active");
        $(".block-modules .modules").removeClass("blocked");
        $(this).addClass('active');
    });

    if (!$('.block-forfait .forfait .vignette').hasClass("active")) {
        $(".block-modules .modules").addClass("blocked");
    } else {
        $(".block-modules .modules").removeClass("blocked");
    }

    // if ($('.block-modules .modules').hasClass("blocked")) {
    //     $(this).hover(function() {
    //         alert("indisponible");
    //     });
    // }

    $('.block-modules .modules .bt-ajoutmod').off().on('click', function () {
        $(this).parent().addClass('blocked');
    });


    if ($(".admin-panel .content.mon-offres .title").text() == "Premium Mode") {
        $(".admin-panel .content.mon-offres .vignette .picto-vign").addClass("premium")
    } else {
        $(".admin-panel .content.mon-offres .vignette .picto-vign").addClass("free")
    }



})(jQuery);