(function ($) {


    var bMobile = $("html").width() < 860 ? true : false;
    var menupadding = bMobile === false ? 110 : 90;


    $(".block-ancre .ancre.secu").click(function () {
        $('html, body').animate({
            scrollTop: $("#ancre-secu").offset().top - menupadding
        }, 1000);
        return false;
    });
    $(".block-ancre .ancre.unique").click(function () {
        $('html, body').animate({
            scrollTop: $("#ancre-unique").offset().top - menupadding
        }, 1000);
        return false;
    });
    $(".block-ancre .ancre.sav").click(function () {
        $('html, body').animate({
            scrollTop: $("#ancre-sav").offset().top - menupadding
        }, 1000);
        return false;
    });

})(jQuery);