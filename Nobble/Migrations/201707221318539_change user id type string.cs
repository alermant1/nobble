namespace Nobble.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeuseridtypestring : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Abonnement", "user_id", c => c.String(nullable: false));
            AddColumn("dbo.Facture", "user_id", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Facture", "user_id");
            DropColumn("dbo.Abonnement", "user_id");
        }
    }
}
