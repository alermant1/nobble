namespace Nobble.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addisoptioninabonnement : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Abonnement", "is_option", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Abonnement", "is_option");
        }
    }
}
