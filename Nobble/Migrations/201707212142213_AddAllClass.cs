namespace Nobble.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAllClass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AbonnementFactures",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        facture_id = c.Int(nullable: false),
                        abonnement_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Abonnement",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.Int(nullable: false),
                        produit_id = c.Int(nullable: false),
                        prix = c.Single(nullable: false),
                        date_achat = c.DateTime(nullable: false),
                        frequence = c.Int(nullable: false),
                        is_actif = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Facture",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.Int(nullable: false),
                        date_facture = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produit",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        libelle = c.String(nullable: false, maxLength: 15),
                        description = c.String(nullable: false),
                        prix = c.Single(nullable: false),
                        is_option = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Produit");
            DropTable("dbo.Facture");
            DropTable("dbo.Abonnement");
            DropTable("dbo.AbonnementFactures");
        }
    }
}
