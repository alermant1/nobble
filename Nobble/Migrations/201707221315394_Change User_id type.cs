namespace Nobble.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeUser_idtype : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Abonnement", "user_id");
            DropColumn("dbo.Facture", "user_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Facture", "user_id", c => c.Int(nullable: false));
            AddColumn("dbo.Abonnement", "user_id", c => c.Int(nullable: false));
        }
    }
}
