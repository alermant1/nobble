namespace Nobble.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.String(nullable: false),
                        nom = c.String(nullable: false),
                        prenom = c.String(nullable: false),
                        adresse = c.String(nullable: false),
                        cdp = c.String(nullable: false),
                        ville = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserInfoes");
        }
    }
}
