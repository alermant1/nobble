﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nobble.Startup))]
namespace Nobble
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
