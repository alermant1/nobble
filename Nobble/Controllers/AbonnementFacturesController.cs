﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nobble.Models;

namespace Nobble.Controllers
{
    public class AbonnementFacturesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AbonnementFactures
        public ActionResult Index()
        {
            return View(db.AbonnementFactures.ToList());
        }

        // GET: AbonnementFactures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbonnementFactures abonnementFactures = db.AbonnementFactures.Find(id);
            if (abonnementFactures == null)
            {
                return HttpNotFound();
            }
            return View(abonnementFactures);
        }

        // GET: AbonnementFactures/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AbonnementFactures/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,facture_id,abonnement_id")] AbonnementFactures abonnementFactures)
        {
            if (ModelState.IsValid)
            {
                db.AbonnementFactures.Add(abonnementFactures);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(abonnementFactures);
        }

        // GET: AbonnementFactures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbonnementFactures abonnementFactures = db.AbonnementFactures.Find(id);
            if (abonnementFactures == null)
            {
                return HttpNotFound();
            }
            return View(abonnementFactures);
        }

        // POST: AbonnementFactures/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,facture_id,abonnement_id")] AbonnementFactures abonnementFactures)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abonnementFactures).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(abonnementFactures);
        }

        // GET: AbonnementFactures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbonnementFactures abonnementFactures = db.AbonnementFactures.Find(id);
            if (abonnementFactures == null)
            {
                return HttpNotFound();
            }
            return View(abonnementFactures);
        }

        // POST: AbonnementFactures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AbonnementFactures abonnementFactures = db.AbonnementFactures.Find(id);
            db.AbonnementFactures.Remove(abonnementFactures);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
