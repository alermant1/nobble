﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nobble.Models;
using System.Diagnostics;
using System.Web.Services;
using Microsoft.AspNet.Identity;

namespace Nobble.Controllers
{
    public class AbonnementsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ProduitsController Produits = new ProduitsController();
        [HttpPost]
        public ActionResult AddAbonnement(string produit_id)
        {
            Produit produit = Produits.GetProduit(Convert.ToInt32(produit_id));
            Abonnement abonnement = new Abonnement();

            if (produit.is_option == false)
            {
                resetAbonnementPrincipal();
            }
            if (!checkUserHaveProduit(Convert.ToInt32(produit_id)))
            {
                abonnement.user_id = User.Identity.GetUserId();
                abonnement.produit_id = Convert.ToInt32(produit_id);
                abonnement.is_option = produit.is_option;
                abonnement.prix = produit.prix;
                abonnement.date_achat = DateTime.Now;
                abonnement.is_actif = false;
                Create(abonnement);
                
            }

            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public Boolean checkUserHaveProduit(int produit_id)
        {
            string user_id = User.Identity.GetUserId();
            return db.Abonnements.Where(Abonnements => Abonnements.user_id == user_id && Abonnements.produit_id == produit_id).Count() > 0;
        }

        public Boolean resetAbonnementPrincipal ()
        {
            string user_id = User.Identity.GetUserId();
            if(db.Abonnements.Where(Abonnements => Abonnements.user_id == user_id && !Abonnements.is_option).Count() > 0)
            {
                Abonnement abonnement = db.Abonnements.Where(Abonnements => Abonnements.user_id == user_id && Abonnements.is_option == false).First();
                db.Abonnements.Remove(abonnement);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
            
        }

        // GET: Abonnements
        public ActionResult Index()
        {
            return View(db.Abonnements.ToList());
        }

        // GET: Abonnements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Abonnement abonnement = db.Abonnements.Find(id);
            if (abonnement == null)
            {
                return HttpNotFound();
            }
            return View(abonnement);
        }

        // GET: Abonnements/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Abonnements/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,user_id,produit_id,prix,date_achat,frequence,is_actif")] Abonnement abonnement)
        {
            if (ModelState.IsValid)
            {
                db.Abonnements.Add(abonnement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(abonnement);
        }

        // GET: Abonnements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Abonnement abonnement = db.Abonnements.Find(id);
            if (abonnement == null)
            {
                return HttpNotFound();
            }
            return View(abonnement);
        }

        // POST: Abonnements/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,user_id,produit_id,prix,date_achat,frequence,is_actif")] Abonnement abonnement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abonnement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(abonnement);
        }

        // GET: Abonnements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Abonnement abonnement = db.Abonnements.Find(id);
            if (abonnement == null)
            {
                return HttpNotFound();
            }
            return View(abonnement);
        }

        // POST: Abonnements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Abonnement abonnement = db.Abonnements.Find(id);
            db.Abonnements.Remove(abonnement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
