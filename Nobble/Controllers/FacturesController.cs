﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nobble.Models;
using Microsoft.AspNet.Identity;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text.html.simpleparser;

namespace Nobble.Controllers
{
    public class FacturesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Factures
        public ActionResult Index()
        {
            return View(db.Factures.ToList());
        }

        // GET: Factures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facture facture = db.Factures.Find(id);
            if (facture == null)
            {
                return HttpNotFound();
            }
            return View(facture);
        }

        [Authorize]
        public ActionResult MesFactures()
        {
            string user_id = User.Identity.GetUserId();

            List<Facture> factures = new List<Facture>();
            factures = db.Factures.Where(Facture => Facture.user_id == user_id).ToList();
            int i = 0;
            foreach (Facture uneFacture in factures)
            {
                factures[i].abonnements = new List<Abonnement>();
                factures[i].prixTotal = 0;
                List<AbonnementFactures> abofacture = db.AbonnementFactures.Where(af => af.facture_id == uneFacture.id).ToList();
                // si abofacture existe => il y a des produits
                if (abofacture.Count() > 0)
                {
                    foreach(AbonnementFactures unAboFacture in abofacture)
                    {
                        Abonnement unAbonnement = db.Abonnements.Find(unAboFacture.abonnement_id);
                        if(unAbonnement != null)
                        {
                            Produit unProduit = db.Produits.Find(unAbonnement.produit_id);
                            unAbonnement.produit = unProduit;
                            factures[i].abonnements.Add(unAbonnement);
                            factures[i].prixTotal += unProduit.prix;
                        }

                    }
                }
                i++;
            }
            return View(factures);
        }

        [Authorize]
        public ActionResult Pdf(int id)
        {
            return File(this.generatePdf(this.getFactureFromUser(id)), "application/pdf", "Facture-" + id + ".pdf");

        }

        private Facture getFactureFromUser(int id)
        {
            string user_id = User.Identity.GetUserId();

            List<Facture> factures = new List<Facture>();
            factures = db.Factures.Where(Facture => Facture.user_id == user_id && Facture.id == id).ToList();
            int i = 0;
            foreach (Facture uneFacture in factures)
            {
                factures[i].abonnements = new List<Abonnement>();
                factures[i].prixTotal = 0;
                List<AbonnementFactures> abofacture = db.AbonnementFactures.Where(af => af.facture_id == uneFacture.id).ToList();
                // si abofacture existe => il y a des produits
                if (abofacture.Count() > 0)
                {
                    foreach (AbonnementFactures unAboFacture in abofacture)
                    {
                        Abonnement unAbonnement = db.Abonnements.Find(unAboFacture.abonnement_id);
                        if (unAbonnement != null)
                        {
                            Produit unProduit = db.Produits.Find(unAbonnement.produit_id);
                            if (unProduit != null)
                            {
                                unAbonnement.produit = unProduit;
                            }
                            factures[i].abonnements.Add(unAbonnement);
                            factures[i].prixTotal += unProduit.prix;
                        }

                    }
                }
                i++;
            }
            if (factures.Count() > 0)
            {
                return factures[0];
            }

            return new Facture();
        }

        private MemoryStream generatePdf(Facture facture)
        {
            MemoryStream stream = new MemoryStream();

            string companyName = "Nobble Corporation";
            string orderNo = "ID-" + facture.id;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4] {
                            new DataColumn("Référence produit", typeof(string)),
                            new DataColumn("Produit", typeof(string)),
                            new DataColumn("Description", typeof(string)),
                            new DataColumn("Prix", typeof(float))});
            foreach (Abonnement unAbonnement in facture.abonnements)
            {
                dt.Rows.Add(
                    "ID-" + unAbonnement.id, 
                    unAbonnement.produit.libelle, 
                    unAbonnement.produit.description,
                    unAbonnement.prix
               );
            }

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                    sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Commande</b></td></tr>");
                    sb.Append("<tr><td colspan = '2'></td></tr>");
                    sb.Append("<tr><td><b>Order No: </b>");
                    sb.Append(orderNo);
                    sb.Append("</td><td align = 'right'><b>Date facturation: </b>");
                    sb.Append(facture.date_facture.ToShortDateString());
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Entreprise: </b>");
                    sb.Append(companyName);
                    sb.Append("</td></tr>");
                    sb.Append("</table>");
                    sb.Append("<br />");

                    //Generate Invoice (Bill) Items Grid.
                    sb.Append("<table border = '1'>");
                    sb.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        sb.Append("<th style = 'background-color: #ffffff;color:#000000'>");
                        sb.Append(column.ColumnName);
                        sb.Append("</th>");
                    }
                    sb.Append("</tr>");
                    foreach (DataRow row in dt.Rows)
                    {
                        sb.Append("<tr>");
                        foreach (DataColumn column in dt.Columns)
                        {
                            sb.Append("<td>");
                            sb.Append(row[column]);
                            sb.Append("</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("<tr><td align = 'right' colspan = '");
                    sb.Append(dt.Columns.Count - 1);
                    sb.Append("'>Total</td>");
                    sb.Append("<td>");
                    sb.Append(dt.Compute("sum(Prix)", ""));
                    sb.Append("</td>");
                    sb.Append("</tr></table>");

                    //Export HTML String as PDF.
                    StringReader sr = new StringReader(sb.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    Image img = Image.GetInstance(Server.MapPath("~/Content/images/commun/logo.png"));
                    img.ScaleToFit(80f, 80f);
#pragma warning disable CS0612 // 'HTMLWorker' is obsolete
#pragma warning disable CS0612 // 'HTMLWorker' is obsolete
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
#pragma warning restore CS0612 // 'HTMLWorker' is obsolete
#pragma warning restore CS0612 // 'HTMLWorker' is obsolete
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.Add(img);

                    htmlparser.Parse(sr);
                    pdfDoc.Close();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=Facture_" + orderNo + ".pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
            }
            stream.Flush();
            stream.Position = 0;
            return stream;
        }



        // GET: Factures/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Factures/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,user_id,date_facture")] Facture facture)
        {
            if (ModelState.IsValid)
            {
                db.Factures.Add(facture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(facture);
        }

        // GET: Factures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facture facture = db.Factures.Find(id);
            if (facture == null)
            {
                return HttpNotFound();
            }
            return View(facture);
        }

        // POST: Factures/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,user_id,date_facture")] Facture facture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(facture).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(facture);
        }

        // GET: Factures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facture facture = db.Factures.Find(id);
            if (facture == null)
            {
                return HttpNotFound();
            }
            return View(facture);
        }

        // POST: Factures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Facture facture = db.Factures.Find(id);
            db.Factures.Remove(facture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
